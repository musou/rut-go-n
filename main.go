package main

import (
	"gon/db"
	"gon/routes"

	"github.com/gin-gonic/gin"
)

func main() {
	db.ConnectToMySql()

	r := gin.Default()

	routes.PingRoute(r)
	routes.UrlRoute(r)

	r.Run()

}

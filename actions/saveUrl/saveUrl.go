package saveUrlAction

import (
	"gon/db"
	"math/rand"
	"net/url"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type UrlRequest struct {
	Url string `json:"url"`
}

func validateRequest(c *gin.Context) UrlRequest {
	var request UrlRequest

	if err := c.BindJSON(&request); err != nil {
		panic("Invalid json")
	}

	_, err := url.ParseRequestURI(request.Url)
	if err != nil {
		panic("Invalid URL")
	}

	return request
}

func randomString(n int) string {
	var letters = []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

	s := make([]rune, n)
	for i := range s {
		s[i] = letters[rand.Intn(len(letters))]
	}
	return string(s)
}

func getKey() string {
	gen := randomString(8)

	existed := db.DB.Where("code = ?", gen).First(&db.Url{})

	if existed.Error == gorm.ErrRecordNotFound {
		return gen
	}

	if existed.Error != nil {
		panic(existed.Error)
	}

	return gen
}

func insertUrl(url string, code string) {
	newUrl := db.Url{Code: code, Url: url}

	db.DB.Create(&newUrl)
}

func SaveUrl(c *gin.Context) gin.H {
	urlRequest := validateRequest(c)

	key := getKey()

	insertUrl(urlRequest.Url, key)

	return gin.H{
		"key": key,
		"url": urlRequest.Url,
	}
}

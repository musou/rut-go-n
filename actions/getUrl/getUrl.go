package getUrlAction

import (
	"fmt"
	"gon/db"
	"regexp"

	"github.com/gin-gonic/gin"
)

type getUrlRequest struct {
	code string `json:"code"`
}

func validateRequest(c *gin.Context) getUrlRequest {
	code := c.Query("code")

	match, err := regexp.MatchString(`^[\d|A-Z]{8}$`, code)
	if err != nil {
		panic("Invalid code")
	}
	if !match {
		panic("Invalid code")
	}

	return getUrlRequest{code: code}
}

func findUrl(code string) string {
	var url db.Url
	db.DB.Model(&db.Url{}).Where("code = ?", code).First(&url)

	fmt.Print(url)

	return url.Url
}

func GetUrl(c *gin.Context) gin.H {
	urlRequest := validateRequest(c)

	url := findUrl(urlRequest.code)

	return gin.H{
		"url": url,
	}
}

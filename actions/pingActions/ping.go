package pingActions

import (
	"time"

	"github.com/gin-gonic/gin"
)

func Ping(c *gin.Context) gin.H {
	return gin.H{
		"time": time.Now().Unix(),
	}
}

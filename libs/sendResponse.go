package libs

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func SendResponse(c *gin.Context, action func(c *gin.Context) gin.H) {
	defer func() {
		if e := recover(); e != nil {
			c.JSON(http.StatusOK, gin.H{
				"success": false,
				"message": e,
			})
		}
	}()

	res := action(c)

	c.JSON(http.StatusOK, gin.H{
		"success": true,
		"data":    res,
	})
}

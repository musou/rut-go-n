package routes

import (
	"gon/actions/pingActions"
	"gon/libs"

	"github.com/gin-gonic/gin"
)

func PingRoute(r *gin.Engine) {
	r.GET("/", func(c *gin.Context) {
		libs.SendResponse(c, pingActions.Ping)
	})
}

package routes

import (
	getUrlAction "gon/actions/getUrl"
	saveUrlAction "gon/actions/saveUrl"
	"gon/libs"

	"github.com/gin-gonic/gin"
)

func UrlRoute(r *gin.Engine) {
	r.POST("/url", func(c *gin.Context) {
		libs.SendResponse(c, saveUrlAction.SaveUrl)
	})

	r.GET("/url", func(c *gin.Context) {
		libs.SendResponse(c, getUrlAction.GetUrl)
	})
}
